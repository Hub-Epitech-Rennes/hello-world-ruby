#!/usr/bin/env ruby

# Ecrire les fonctions ici.

def get_name()
	puts "Whats your name ?"
	name = gets.chomp #attend que quelque chose soit ecrit et est save dans name. Pour l'afficher utiliser puts.
	return name
end

def hello
	puts "Hello World"
end

def hello_me(name)
	puts "Hello #{name}, how are you ?"
end

def main
	name = get_name 

	if name.empty?
		hello
	else
		hello_me(name)
	end
# Ecrire ici le reste du programme.

end

main
